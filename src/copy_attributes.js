//
//  copies all available attributes named in att_names from from_el to to_el
//
export default copyAttributes

function copyAttributes(from_el, to_el, att_names){
  att_names.forEach((att)=>{
    let val = from_el.getAttribute(att)
    val && to_el.setAttribute(att, val)
  })
}
