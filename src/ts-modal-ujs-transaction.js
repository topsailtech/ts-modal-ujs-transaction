import dialogPolyfill from 'dialog-polyfill'
import 'dialog-polyfill/dist/dialog-polyfill.css'
import { default as copyAttributes } from './copy_attributes'
import { update_html } from 'wc-util'

class TsModalUjsTransaction extends HTMLAnchorElement {
  connectedCallback(){
    this.setAttribute("data-remote", true)
    this.addEventListener("ajax:send", ajaxSend.bind(this))
    this.addEventListener("ajax:success", ajaxSuccess.bind(this))
  }
}

function ajaxSend(ev){
  this.dialog_body = attach_dialog.bind(this)()
}

function ajaxSuccess(ev){
  const xhr = event.detail[2]
  update_html(this.dialog_body, xhr.responseText)
}

function attach_dialog(){
  const dialog = document.createElement("dialog")
  dialog.classList.add("ts_modal_ujs_transaction_dialog") // styling in cruddy_views
  dialogPolyfill.registerDialog(dialog); // for non-Chrome

  dialog.addEventListener("close", ()=> dialog.remove())
  dialog.innerHTML = `
    <div class="ts_dialog_title">${this.title}</div>
    <ts-dialog-transaction class="ts_dialog_body">
      Loading...
    </ts-dialog-transaction>`

  const dialog_body = dialog.querySelector("ts-dialog-transaction")
  copyAttributes(this, dialog_body, ["keep-open", "append", "prepend", "update", "replace"])

  document.body.append(dialog)
  dialog.showModal()

  return dialog_body
}

customElements.define('ts-modal-ujs-transaction', TsModalUjsTransaction, { extends: "a" })
