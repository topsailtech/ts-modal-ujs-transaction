/*
    <dialog>
      <ts-dialog-transaction></ts-dialog-transaction>
    </dialog>

    <dialog>
      <ts-dialog-transaction keep-open></ts-dialog-transaction>
    </dialog>

    <dialog>
      <ts-dialog-transaction append="a_dom_id"></ts-dialog-transaction>
    </dialog>

    <dialog>
      <ts-dialog-transaction prepend="a_dom_id"></ts-dialog-transaction>
    </dialog>

    if an ajax:error bubbles up
      - replace my own content with the ajax:error payload

    if an ajax:success bubbles up
      (A) if response has header "ts-deleted", delete the DOM element with that ID and close the dialog
      (B) if I have attribute "keep-open", or event.target has attribute data-keep-open,
          replace my own content with the ajax:success payload
      (C) if neither (A) nor (B)
        (C1) and I have an attribute "replace" or "append" or "prepend" or "update",
             replace/append/prepend/update the DOM element with ID given in such attribute with the ajax:success payload
        (C2) no attribute replace/append/prepend/update,
             update DOM elements identified by root elements (with ID) of payload fragment
*/

import { update_html } from 'wc-util'
import { default as updateDomFromFragmentString } from 'ts-ujs-dom-update/src/update_dom_from_fragment_string.js'

class TsDialogTransaction extends HTMLElement {
  connectedCallback(){
    // wire up callbacks
    this.addEventListener("ajax:error",   updateAfterAjaxError.bind(this))
    this.addEventListener("ajax:success", updateAfterAjaxSuccess.bind(this))
    this.addEventListener("ajax:success", closeDialog.bind(this))
  }
}

function updateAfterAjaxError(event){
  update_html(this, event.detail[2].responseText)
}

function updateAfterAjaxSuccess(event){
  let xhr = event.detail[2]
  if (xhr.getResponseHeader("ts-deleted")) {
    let delete_el = document.getElementById(xhr.getResponseHeader("ts-deleted"))
    delete_el && delete_el.remove()
  } else if (event.target.dataset.keepOpen || this.hasAttribute("keep-open")){
    update_html(this, xhr.responseText)
  } else {
    updateDomFromFragmentString(
      xhr.responseText,
      {
        append:  this.getAttribute("append"),
        prepend: this.getAttribute("prepend"),
        update:  this.getAttribute("update"),
        replace: this.getAttribute("replace")
      }
    )
  }
}

function closeDialog(event){
  if (event.detail[2].getResponseHeader("ts-deleted") ||
       !(event.target.dataset.keepOpen || this.hasAttribute("keep-open")))
    this.closest("dialog").close()
}

customElements.define('ts-dialog-transaction', TsDialogTransaction)
