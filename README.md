# ts-modal-ujs-transaction

A Custom Element to kick off a modal dialog. 
Remote UJS requests will be performed inside that dialog, until _Http Status_ *success* is returned and no expolict _data-keep-open=true_ was requested.
When done (dialog closes), the last request result will replace or update local DOM element(s).

## Installation

```
yarn add git+ssh://git@bitbucket.org:topsailtech/ts-modal-ujs-transaction.git
```
In your pack (as javascript *and* a css resource) ```require('ts-modal-ujs-transaction')```.

## Usage

To replace exsiting DOM elements that have the same DOM *id* as the returned elements:
```html
<a href="some_url_for_dialog_content" is="ts-modal-ujs-transaction">Open this dialog</a>
```

To append new elements to an existing DOM element:
```html
<a href="some_url_for_dialog_content" is="ts-modal-ujs-transaction" append="some_item_list">Open this dialog</a>
```

To prepend new elements to an existing DOM element:
```html
<a href="some_url_for_dialog_content" is="ts-modal-ujs-transaction" prepend="some_item_list">Open this dialog</a>
```
To remove an element in the existing DOM, the response should include a header *ts-deleted*, with the DOM ID as the value.

The dialog will close when a *success*ful UJS request is being done. Otherwise, UJS result fragments will update the dialog.

You can force the dialog to stay open even for a successful UJS request by adding a data attribute _data-keep-open=true_ to the triggering link or form.

When the dialog closes (due to sucessful UJS request), local DOM elements will get replaced/appended/prepended with the UJS reply.
